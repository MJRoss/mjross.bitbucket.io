//When element loses focus, call appropriate function
var elName = document.querySelector('#name');
var elEmail = document.querySelector('#email');
var elPhone = document.querySelector('#phone');
var elMsg = document.querySelector('#message');

elName.addEventListener('blur', function(){
    //Get the feedback element
    var elMsg = document.querySelector('#feedbackName');

    //Check if username is valid
    if(this.value.length < 3){
        elMsg.innerHTML = "You must enter a name with at least 3 characters!"; //Set the feedback message
    } else {
        elMsg.innerHTML = ""; //Clear the message
    }
}, false);

elMsg.addEventListener('blur', function(){
    //Get the feedback element
    var elMsg = document.querySelector('#feedbackMsg');

    //Check if the user entered a message
    if(this.value.length < 1 || this.value.length > 150){
        elMsg.innerHTML = "You must enter a message between 1 and 150 characters!";
    } else {
        elMsg.innerHTML = "";
    }
}, false);

window.addEventListener('load', function() {
    document.querySelector('#name').value = "";
    document.querySelector('#email').value = "";
    document.querySelector('#phone').value = "";
    document.querySelector('#message').value = "";
}, false);

