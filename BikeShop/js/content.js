    //Create the response object
    var xhr = new XMLHttpRequest();

    xhr.onload = function(){   //After load completes
        /*if(xhr.status === 200){ */    //If server status is ok
        var responseObject = JSON.parse(xhr.responseText);

        var productsContent = '';
        var eventsContent = '';
        var postsContent = '';
        var benefitsContent = '';

        for(var i = 0; i < responseObject.products.length; i++){
            productsContent += '<div class=product>';
            productsContent += '<img src=' + responseObject.products[i].imageURL + ' ';
            productsContent += 'alt=' + responseObject.products[i].title + ' />';
            productsContent += '<div class="desc"><p><b>' + responseObject.products[i].title + '</b><br />';
            productsContent += responseObject.products[i].description + '</p></div>';
            productsContent += '</div>';
        }

        for(var j = 0; j < responseObject.benefits.length; j++) {
            benefitsContent += '<div class=benefit>';
            if(j == 0){
                benefitsContent += '<img src=images/truck.png alt=Truck />';
            }
            else if(j == 1){
                benefitsContent += '<img src=images/wrench.png alt=Wrench />';
            }
            else if(j == 2){
                benefitsContent += '<img src=images/truck.png alt=Envelope />';
            }
            benefitsContent += '<p><strong>' + responseObject.benefits[j].title + '</strong><br />';
            benefitsContent += responseObject.benefits[j].description + '</p>';
            benefitsContent += '</div>';
        }

        for(var k = 0; k < responseObject.events.length; k++){
            eventsContent += '<div class=event>';
            eventsContent += '<p><strong>' + responseObject.events[k].title + '</strong><br />';
            eventsContent += responseObject.events[k].date + '<br />';
            eventsContent += responseObject.events[k].location + '<br />';
            eventsContent += responseObject.events[k].text + '</p>';
            eventsContent += '</div>';
        }

        for(var l = 0; l < responseObject.posts.length; l++){
            postsContent += '<div class=post>';
            postsContent += '<img src=' + responseObject.posts[l].imageURL + ' ';
            postsContent += 'alt=' + responseObject.posts[l].title + ' />';
            postsContent += '<p><strong>' + responseObject.posts[l].title + '</strong><br />';
            postsContent += responseObject.posts[l].postDate + '<br />';
            postsContent += responseObject.posts[l].text + '</p>';
            postsContent += '</div>';
        }


        //Update the page with product content
        var featured = document.querySelector('#featured');
        var memberBenefits = document.querySelector('#memberBenefits');
        var currentEvents = document.querySelector('#currentEvents');
        var posts = document.querySelector('#posts');

        if(featured != null){
            featured.innerHTML = productsContent;
        }

        if(memberBenefits != null){
            memberBenefits.innerHTML = benefitsContent;
        }

        if(currentEvents != null){
            currentEvents.innerHTML = eventsContent;
        }

        if(posts != null){
            posts.innerHTML = postsContent;
        }

        //}
    };

    xhr.open('GET', 'data/data.json', true);    //Prepare the request
    xhr.overrideMimeType('application/json');
    xhr.send('null');       //Send the request
//}
